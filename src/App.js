import React, { Component } from 'react';
import TaskForm from './components/TaskForm'
import Control from './components/Control'
import TaskList from './components/TaskList'
import './App.css'

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      tasks: [],
      isDisplayForm: false
    }
  }
  componentWillMount() {
    if(localStorage && localStorage.getItem('tasks')) {
      let tasks = JSON.parse(localStorage.getItem('tasks'))
      this.setState({
        tasks: tasks
      })
    }
  }
  generateData = () => {
    let tasks = [
      {
        id: this.generateID(),
        name: 'Hoc lap trinh',
        status: true
      },
      {
        id: this.generateID(),
        name: 'Hoc ke toan',
        status: false
      },
      {
        id: this.generateID(),
        name: 'Hoc tai chinh',
        status: true
      }
    ]
    localStorage.setItem('tasks', JSON.stringify(tasks));
  }
  s4() {
    return Math.floor((1+Math.random() * 0x10000)).toString(6).substring(1);
  }
  generateID() {
    return this.s4() + this.s4() + '-' + this.s4() + '-' + this.s4() + '-' + this.s4() + '-' + this.s4() + this.s4() + this.s4();
  }
  onToggelForm = () => {
    this.setState({
      isDisplayForm: !this.isDisplayForm
    })
  }
  setCloseForm = () => {
    this.setState({
      isDisplayForm: false
    })
  }
  onSubmitForm = (data) =>{
    let {tasks} = this.state;
    data.id = this.generateID();
    tasks.push(data)
    this.setState({
      tasks: tasks
    })
    localStorage.setItem('tasks', JSON.stringify(tasks));
  }
  onUpdateStatus = (id) => {
    let {tasks} = this.state;
    let index = this.findIndex(id);
    if(index !== -1) {
      tasks[index].status = !tasks[index].status;
      this.setState({
        tasks: tasks
      })
      localStorage.setItem('tasks', JSON.stringify(tasks));
    }
  }
  findIndex = (id) => {
    let {tasks} = this.state;
    let result = -1;
    tasks.forEach((task, index) => {
      if(task.id === id) {
        result = index;
      }
    })
    return result;
  }
  onDelete = (id) => {
    let {tasks} = this.state;
    let index = this.findIndex(id);
    if(index !== -1) {
      tasks.splice(index, 1)
      this.setState({
        tasks: tasks
      })
      localStorage.setItem('tasks', JSON.stringify(tasks));
    }
  }
  
render() {
  let { isDisplayForm } = this.state;
  let elementTaskForm = isDisplayForm ? <TaskForm onCloseForm={this.setCloseForm} onSubmitForm={this.onSubmitForm}></TaskForm> : '';
return (
<div className="App">
  <div className="container">
    <div className="text-center">
      <h1>Quản Lý Công Việc</h1>
      <hr />
    </div>
    <div className="row">
      <div className={ isDisplayForm ? 'col-xs-4 col-sm-4 col-md-4 col-lg-4' : '' }>
        { elementTaskForm }
      </div>
      <div className={ isDisplayForm ? 'col-xs-8 col-sm-8 col-md-8 col-lg-8' : 'col-xs-12 col-sm-12 col-md-12 col-lg-12'}>
        <button type="button" className="btn btn-primary"
        onClick={ this.onToggelForm }>
          <span className="fa fa-plus mr-5"></span>Thêm Công Việc
        </button>
        &nbsp;&nbsp;&nbsp;
        <button type="button" className="btn btn-primary" onClick={ this.generateData }>
          <span className="fa fa-plus mr-5"></span>Generate Data
        </button>
        <Control></Control>
        <div className="row mt-15">
          <div className="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <TaskList tasks={this.state.tasks} onUpdateStatus={this.onUpdateStatus} onDelete={this.onDelete}></TaskList>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
);
}
}

export default App;